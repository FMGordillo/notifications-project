import withPWA from "next-pwa";
/**
 * Run `build` or `dev` with `SKIP_ENV_VALIDATION` to skip env validation. This is especially useful
 * for Docker builds.
 */
await import("./src/env.js");

const haber = withPWA({
  dest: "public",
  register: true,
  skipWaiting: true,
});

export default haber({
  reactStrictMode: true,
  ignorePatterns: ["public"],
});
